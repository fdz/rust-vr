[Trello Baord](https://trello.com/b/abf2DrZW/rust-vr)

---

[Markdown Cheatsheet](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet)

# Some useful, Unity related links
[Unity's Manual: Terrain Engine](https://docs.unity3d.com/Manual/script-Terrain.html)  
[Unity's Asset Store: Easy Roads (free)](https://assetstore.unity.com/packages/3d/characters/easyroads3d-free-987)  
https://assetstore.unity.com/packages/tools/audio/free-footsteps-system-47967  
https://assetstore.unity.com/packages/tools/terrain/mass-tree-placement-21865  
https://assetstore.unity.com/packages/templates/tutorials/vr-shooting-range-photon-85121  

# Envioroment Assets for prototyping
https://assetstore.unity.com/packages/3d/environments/nature-starter-kit-2-52977  
https://assetstore.unity.com/packages/3d/environments/landscapes/free-island-collection-104753  
https://assetstore.unity.com/packages/3d/environments/landscapes/realistic-terrain-collection-lite-47726  
https://assetstore.unity.com/packages/3d/environments/roadways/lake-race-track-55908  
There was that asset for using google maps.

# Networking
NetworkIdentity-Component  
SyncTransform  
Server.Spawn()  
Information from client to server: [Command] CmdFunction()   
Information form server to client: [ClientRPC] RPCFunction()  
[SyncVar] Can be implemented with RPC, useful for simple stuff   
Register Prefab  

https://docs.unity3d.com/Manual/UNetManager.html  
https://docs.unity3d.com/Manual/UNetSetup.html  
https://docs.unity3d.com/ScriptReference/MasterServer-dedicatedServer.html    
