﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

namespace Name
{
    public class FindMyIP : MonoBehaviour
    {
        /// <summary>
        /// Start is called on the frame when a script is enabled just before
        /// any of the Update methods is called the first time.
        /// </summary>
        void Start()
        {
            Debug.Log(Network.player.ipAddress);
            StartCoroutine(CheckIP());
        }

        IEnumerator CheckIP()
        {
            var myExtIPWWW = new WWW("http://checkip.dyndns.org");
            if (myExtIPWWW != null)
            {
                yield return myExtIPWWW;
                var myExtIP = myExtIPWWW.text;
                myExtIP = myExtIP.Substring(myExtIP.IndexOf(":") + 1);
                myExtIP = myExtIP.Substring(0, myExtIP.IndexOf("<"));
                print("checkip.dyndns.org -- " + myExtIP);
            }
        }
    }// end of class
}// end of namespace