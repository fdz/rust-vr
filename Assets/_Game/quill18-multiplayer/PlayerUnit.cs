﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

namespace quill18
{
    // a PlayerUnit is a unit controlled by a player
    // this could be a character in an FPS, a zergling in a RTS,
    // or a scout in a TBS
    public class PlayerUnit : NetworkBehaviour
    {
        Vector3 velocity;
        Vector3 bestGuessPosition;// the position we think is most correct for this player
                                  // NOTE: if we are the authority, theb this will be the exactly the same as transform.position

        // this is a constantly updated value abour our latency to the server
        // i.e. how many second it takes for us to receive a one-way message
        // TODO: This should probably something we get from the PlayerConnectionObject
        float ourLatency;

		// the higher the value, the faster our local position will match the best guess position
		float latencySmoothingFactor = 10;
		
		void Start()
		{
			Debug.Log("PlayerUnit.Start::hasAuthority == " + hasAuthority);
		}

        void Update()
        {
            // code running right here is running for all version for this object,
            // even if it's not the authoratitive copy
            // but even if we're NOT the owner, we are trying to PREDICT hwere the object
            // should be right now, based on the las velocity update.
			
			Debug.Log("PlayerUnit.Update::hasAuthority == " + hasAuthority);

            if (hasAuthority == false)
            {
				// we aren't the authority for this boject, but we still need to update
				// our local position for this object based on our best guess of where
				// it probably is on the owning player's sceen

				bestGuessPosition = bestGuessPosition + ( velocity * Time.deltaTime);

				// instead of TELEPORTING our position to the best guess's position, we
				// can smoothly lerp to it.

				transform.position = Vector3.Lerp( transform.position, bestGuessPosition, Time.deltaTime * latencySmoothingFactor );

                return;
            }

			// if we get to here, we are the authoritative owner of this object
            transform.Translate(velocity * Time.deltaTime);

            if (Input.GetKeyDown(KeyCode.Space))
            {
                this.transform.Translate(0, 1, 0);
            }
            if (Input.GetKeyDown(KeyCode.D))
            {
                Destroy(gameObject);
            }

            if (true)//some input
            {
                // the player is asking us to change our direction/speed (i.e. velocity)
                velocity = new Vector3(1, 0, 0);
                CmdUpdateVelocity(velocity, transform.position);// 0 ms delay, send information to server
            }
        }

        [Command]
        void CmdUpdateVelocity(Vector3 v, Vector3 p)// minimize information, like sending a float for velocity or something like that
        {
            // because of latency, the Cmd takes time to reach the server (delay/latency)
            // on the server
            velocity = v;
            transform.position = p;

            // if we know what our current latency is, we could do something like this:
            // transform.position = p + ( v * (thisPlayerLatencyToServer) );

            // now let the clients know the correct position of this object
            RpcUpdateVelocity(velocity, transform.position);// latency "doubles up" since now the server is sending information to the clients
        }

        [ClientRpc]
        void RpcUpdateVelocity(Vector3 v, Vector3 p)
        {
            // on a client
            if (hasAuthority)
            {
                // this is my own object. I "should" already have the most accurate
                // position/velocity (possibly more "accurate") than the server
                // depending on the game, i MIGHT want to change to match this info
                // from the server, even though that might look a little wonky to the user.

                // let's assume for now that we're just going to ignore the message from the server.
                return;
            }
            // i am a non-authoratative cleint, so i definetly need to listen to the server

            // if we know what our current latency is, we could do something like this:
            // transform.position = p + ( v * (ourLatency + theirLatency) );
            // transform.position = p + ( v * (ourLatency) );

            //transform.position = p;
            velocity = v;
            bestGuessPosition = p + (v * (ourLatency));

            // now position of player one is as close as possible on all player's screens

            // IN FACT, we don't want to directly update transform.position, because then
            // players will keep teleporint/blinking as updates come in. It looks dumb.
        }
    }// end of class
}