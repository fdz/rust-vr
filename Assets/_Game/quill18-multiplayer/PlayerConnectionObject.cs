﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

namespace quill18
{
    public class PlayerConnectionObject : NetworkBehaviour
    {
        void Start()
        {
            // is this actually my local PlayerObject
            if (isLocalPlayer == false)
            {
                return;
            }

            // Since the PlayerObject is invisible and not part of the world
            // give me something physical to move around

            Debug.Log("PlayerObject::Start -- Spawning my own personal unit.");

            // Instantiate() only creates an object on the LOCAL COMPUTER
            // even if it has a NetworkIdentity is still will not exit on
            // the network (and therefore not on any other client) UNLESS
            // NetworkServer.Spawn() is called on this object

            // Instantiate(playerUnitPrefab);

            // Command the server to SPAWN our unit
            CmdSpawnMyUnit();
        }

        public GameObject playerUnitPrefab;
        [SyncVar(hook="OnPlayerNameChanged")]// Sync vars are varaibles where if their value changes on the SERVER, then all clients
        // are automatically informed of the new value
        public string playerName = "Anonymous";

        void Update()
        {
            if (isLocalPlayer == false)
            {
                return;
            }
            // Remember: Update runs on EVERYONE'S computer,
            // wheter or not they own this particular PalyerObject
			if ( Input.GetKeyDown(KeyCode.S) )
			{
				CmdSpawnMyUnit();
			}

            if ( Input.GetKeyDown(KeyCode.Q) ) {
                string n = "Quill " + Random.Range(1, 100);
                Debug.Log("Sending the server a to change our name to: " + n);
                CmdChangePlayerName(n);
            }
        }

        void OnPlayerNameChanged(string newName)
        {
            Debug.Log("OnPlayerNameChanged: OldName: "+ playerName + ", NewName: " +  newName);

            // WARNING: If you use a hook on a SyncVar, then our local value does not get automatically updated
            playerName = newName;
            
            gameObject.name = "PlayerConnectionObject["+newName+"]";
        }

        /////////////////// COMMANDS
        // Commands are special functions that ONLY get executed on the SERVER
        
        [Command]
        void CmdSpawnMyUnit()
        {
            // We are guaranteed to be on the server right now.
            GameObject go = Instantiate(playerUnitPrefab);

			//go.GetComponent<NetworkIdentity>().AssignClientAuthority( connectionToClient );

            // now that the object exists on the server, propagate it to all
            // the client (and also wire up the network identity)
			//NetworkServer.Spawn(go);
            NetworkServer.SpawnWithClientAuthority(go, connectionToClient);
        }

        [Command]
        void CmdChangePlayerName(string n)
        {
            Debug.Log("CmdChangePlayerName: " + n);
            // maybe we should check that the name doesn't have any blacklisted words it?
            // if there's a bad word in the name, do we just ignore this request or nothing?
            // or do we still call the rpc but with the original name?

            playerName = n;

            // tell all the client what this player's name now is

            //RpcChangePlayerName(playerName);
        }

        /////////////////// COMMANDS
        // RPC are special functions that ONLY get executed on the CLIENTS
        // [ClientRpc]
        // void RpcChangePlayerName(string n)
        // {
        //     Debug.Log("RpcChangePlayerName: we were asked to change the player name on a particular PlayerConnectionObject: " + n);
        //     playerName = n;
        // }
    }
}