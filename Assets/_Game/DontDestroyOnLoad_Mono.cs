﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Name
{
    public class DontDestroyOnLoad_Mono : MonoBehaviour
    {
        void Awake()
        {
            DontDestroyOnLoad(this.gameObject);
            Destroy(this);
        }
    }// end of class
}// end of namespace