﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

namespace ConstructionSystem
{
    [System.Serializable]
    public class SlotForBuildable
    {
        public System.Type acceptedType;
        Transform ownerTransform;
        public Vector3 localPosition;
        public Vector3 worldPosition { get { return ownerTransform.position + (ownerTransform.rotation * localPosition); } }
        public Buildable obj;
        public bool isFree { get { return obj == null; } }

        public SlotForBuildable(System.Type acceptedType, Transform ownerTrans, Vector3 localPos)
        {
            this.acceptedType = acceptedType;
            ownerTransform = ownerTrans;
            localPosition = localPos;
        }
    }// end of slot
}// end of namespace