﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

namespace ConstructionSystem
{
    /// <summary>The basic functions that are need it be overriden are: Placement_GetBuildable(), and the variable 'canBePlacendOnFloor'</summary>
    public abstract class Buildable : NetworkBehaviour
    {
        // ========================= STATIC/CONST

        public const float TILE_WIDTH = 2;
        public const float TILE_HEIGHT = 2.5f;

        public static SlotForBuildable GetNearestSlot(Buildable someBuidable, Vector3 worldPosition, System.Type withAcceptedType)
        {
            SlotForBuildable rSlot = null;
            float minDist = Mathf.Infinity;
            foreach (var slot in someBuidable.slots)
            {
                if (slot.acceptedType != withAcceptedType)
                {
                    continue;
                }
                float curDist = Vector3.Distance(slot.worldPosition, worldPosition);
                if (curDist < minDist)
                {
                    minDist = curDist;
                    rSlot = slot;
                }
            }
            return rSlot;
        }

        // ========================= VARAIBLES

        public bool canBePlacedOnFloor = false;
        public bool lookAtWhatSnaps = false;
        [SerializeField] public SlotForBuildable[] slots;

        // ========================= ABSTRACT

        #region Placement
        /// <summary>Returns wheter it can be placed or not</summary>
        public virtual bool Placement(RaycastHit[] hits, out System.Action OnApplyPlacementCallback)
        {
            if (hits.Length == 0)
            {
                OnApplyPlacementCallback = null;
                return false;
            }

            OnApplyPlacementCallback = null;

            RaycastHit chosenHit;
            var chosenBuildable = Placement_GetBuildable(hits, out chosenHit);

            if (chosenBuildable == null)
            {
                return Placement_OnGround(chosenHit, out OnApplyPlacementCallback);
            }
            else
            {
                return Placement_SnapToBuildable(chosenHit, chosenBuildable, out OnApplyPlacementCallback);
            }
        }

        /// <summary> override this for checking what this can be attached to </summary>
        protected virtual Buildable Placement_GetBuildable(RaycastHit[] hits, out RaycastHit chosenHit)
        {
            //return GetBuildable<Buildable>(hits, out chosenHit);
            chosenHit = hits[0];
            Buildable chosenBuildable = null;

            // SlotForBuidable have a reference to the type of buildables that can snap to the owner
            // so, on the raycast, find a buildable, check the slots of the buildable found for that has a
            // reference to the type of the buildable that is gonna be placed

            foreach (var hit in hits)
            {
                Buildable hit_buildable = hit.collider.GetComponent<Buildable>();
                if (hit_buildable == null)
                {
                    continue;
                }
                foreach (var slot in hit_buildable.slots)
                {
                    if (slot.acceptedType == this.GetType())
                    {
                        chosenHit = hit;
                        chosenBuildable = hit_buildable;
                        break;
                    }
                    else if (hit.collider.gameObject.layer == Layers.intGround)
                    {
                        chosenHit = hit;
                    }
                }
            }

            return chosenBuildable;
        }

        protected virtual bool Placement_OnGround(RaycastHit chosenHit, out System.Action OnApplyPlacement)
        {
            Placement_OnGround_SetTransform(chosenHit);
            OnApplyPlacement = null;
            if (canBePlacedOnFloor)
            {
                return CheckForFreeSpace(this, .55f);// a little MORE than half (.5f), so it doesn't clip other Buildables
            }
            return canBePlacedOnFloor;
        }

        protected virtual void Placement_OnGround_SetTransform(RaycastHit chosenHit)
        {
            transform.position = chosenHit.point;
        }

        protected virtual bool Placement_SnapToBuildable(RaycastHit chosenHit, Buildable chosenBuildable, out System.Action OnApplyPlacementCallback)
        {
            SlotForBuildable chosenSlot;
            Placement_SnapToBuildable_SetTransform(chosenHit, chosenBuildable, out chosenSlot);

            if (CheckForFreeSpace(chosenBuildable, .45f) == false)// a little LESS than half (.5f), so i don't have to program more stuff
            {
                OnApplyPlacementCallback = null;
                return false;// space is not free
            }

            OnApplyPlacementCallback = () =>
            {
                Placement_Apply(chosenHit, chosenBuildable, chosenSlot);
            };

            return chosenSlot.isFree;
        }



        protected virtual void Placement_SnapToBuildable_SetTransform(RaycastHit chosenHit, Buildable chosenBuildable, out SlotForBuildable chosenSlot)
        {
            chosenSlot = GetNearestSlot(chosenBuildable, chosenHit.point, this.GetType());
            transform.position = chosenSlot.worldPosition;

            if (lookAtWhatSnaps == false)
            {
                transform.rotation = chosenBuildable.transform.rotation;
            }
            else
            {
                Quaternion lookRotation = Quaternion.LookRotation(chosenBuildable.transform.position - this.transform.position);
                Vector3 newRotation = new Vector3(0, lookRotation.eulerAngles.y, 0);
                transform.rotation = Quaternion.Euler(newRotation);
            }
        }

        protected virtual void Placement_Apply(RaycastHit chosenHit, Buildable chosenBuildable, SlotForBuildable chosenSlot)
        {
            if (chosenSlot.isFree)// the reference for foundations it is not really necesary (i think), but i already made it so i have a starting point, when destroying stuff
            {
                chosenSlot.obj = this;
                //chosenSlot.netId = this.netId.Value;
            }
        }
        #endregion//Placement

        protected virtual bool CheckForFreeSpace(Buildable excludeBuidable, float sizeFactor = .5f)
        {
            Collider[] colliders = OverlapsColliders(transform.position, sizeFactor);
            foreach (var overlapedCollider in colliders)
            {
                var overlapedBuidable = overlapedCollider.gameObject.GetComponent<Buildable>();
                if (overlapedCollider == this.collider || overlapedBuidable == null || overlapedBuidable == excludeBuidable)
                {
                    continue;
                }
                return false;
            }
            return true;
        }

        /// <summary>If **returns** *null*, means that raycast hitted ground.</summary>
        protected virtual BuildableThing GetBuildable<BuildableThing>(RaycastHit[] hits, out RaycastHit out_hit) where BuildableThing : Buildable
        {
            // Get correct hit
            RaycastHit chosenHit = hits[0];
            BuildableThing chosenBuildable = null;

            foreach (var hit in hits)
            {
                if (hit.collider == this.collider)
                {
                    continue;
                }
                // Check for component
                chosenBuildable = hit.collider.GetComponent<BuildableThing>();
                if (chosenBuildable != null)
                {
                    chosenHit = hit;
                    break;
                }
                // Check for ground
                if (hit.collider.gameObject.layer == Layers.intGround)
                {
                    chosenHit = hit;
                }
            }

            Debug.DrawLine(chosenHit.point, chosenHit.point + Vector3.up, Color.red);

            out_hit = chosenHit;
            return chosenBuildable;
        }
        // ========================= NON-ABSTRACT

        protected new Collider collider;
        protected MeshRenderer[] meshRenderers;

        void Start()
        {
            GetComponents();
        }

        void OnDrawGizmosSelected()
        {
            float size = .05f;
            Gizmos.color = Color.green;
            foreach (var slot in slots)
            {
                Gizmos.DrawSphere(slot.worldPosition, size);
            }
        }

        // ========================= VIRTUAL

        protected BuildingLevels buildingLvl;
        protected BuildingObjects buildingObj;

        public void Init(BuildingLevels buildingLevel, BuildingObjects buildingObject)
        {
            buildingLvl = buildingLevel;
            buildingObj = buildingObject;
        }

        public virtual void Demolish()
        {
            Destroy(this.gameObject);

            foreach (var slot in slots)
            {
                if (slot.obj == null)
                {
                    continue;
                }
                slot.obj.Demolish();
            }
        }

        protected virtual Collider[] OverlapsColliders(Vector3 worldPosition, float sizeFactor = .5f)
        {
            BoxCollider boxCollider = this.collider as BoxCollider;
            Collider[] colliders = Physics.OverlapBox(worldPosition + boxCollider.center, boxCollider.size * sizeFactor, transform.rotation);
            if (colliders.Length == 0)
            {
                Debug.Log("Overlap found nothing");
            }
            return colliders;
        }

        public virtual void GetComponents()
        {
            collider = GetComponent<Collider>();
            meshRenderers = transform.GetChild(0).GetComponentsInChildren<MeshRenderer>();
        }

        public virtual void SetColliderEnabled(bool value)
        {
            if (collider == null)
            {
                Debug.LogError("Missing coolider");
                return;
            }
            collider.enabled = value;
        }

        public virtual void SetMaterials(Material otherMaterial)
        {
            if (meshRenderers == null)
            {
                Debug.LogError("Missing meshRenderer");
                return;
            }
            for (int mr = 0; mr < meshRenderers.Length; mr++)
            {
                meshRenderers[mr].material = otherMaterial;
            }
        }

        public virtual void ResetMaterials()
        {
            GameObject prefab = BuildingPrefabs.GetPrefab(buildingLvl, buildingObj);
            MeshRenderer[] prefabMeshRenreders = prefab.transform.GetChild(0).GetComponentsInChildren<MeshRenderer>();
            this.SetMaterials(prefabMeshRenreders);
        }

        public virtual void SetMaterials(MeshRenderer[] otherMeshRenderers)
        {
            if (meshRenderers == null)
            {
                Debug.LogError("Missing meshRenderer");
                return;
            }
            for (int mr = 0; mr < meshRenderers.Length; mr++)
            {
                meshRenderers[mr].material = otherMeshRenderers[mr].sharedMaterial;
            }
        }

        // ============================
    }//end of class
}//end of namespace