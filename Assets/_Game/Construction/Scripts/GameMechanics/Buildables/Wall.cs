﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ConstructionSystem
{
    public class Wall : Buildable
    {
        void Start()
        {
            lookAtWhatSnaps = true;
            slots = new SlotForBuildable[]
            {
                new SlotForBuildable( typeof(Floor), this.transform, Vector3.forward * ( TILE_WIDTH * .5f ) + Vector3.up * TILE_HEIGHT ),
                new SlotForBuildable( typeof(Floor), this.transform, -Vector3.forward * ( TILE_WIDTH * .5f ) + Vector3.up * TILE_HEIGHT  )
            };
        }
    }//end of class
}//end of namespace