﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ConstructionSystem
{
    public class Foundation : Buildable
    {
        // ========================= STATIC

        protected void ConnectFoundationsReferences(Foundation foundationA, Foundation foundationB)
        {
            SlotForBuildable slotA = Buildable.GetNearestSlot(this, foundationB.transform.position, typeof(Foundation));
            SlotForBuildable slotB = Buildable.GetNearestSlot(this, foundationA.transform.position, typeof(Foundation));

            slotA.obj = foundationB;
            slotB.obj = foundationA;
        }

        // ========================= VARIABLES

        void Start()
        {
            canBePlacedOnFloor = true;

            slots = new SlotForBuildable[]
            {
                new SlotForBuildable( typeof(Wall), this.transform, Vector3.forward * TILE_WIDTH* .5f ),
                new SlotForBuildable( typeof(Wall), this.transform, -Vector3.forward * TILE_WIDTH* .5f ),
                new SlotForBuildable( typeof(Wall), this.transform, Vector3.right * TILE_WIDTH* .5f ),
                new SlotForBuildable( typeof(Wall), this.transform, -Vector3.right * TILE_WIDTH * .5f ),
                new SlotForBuildable( typeof(Foundation), this.transform, Vector3.forward * TILE_WIDTH ),
                new SlotForBuildable( typeof(Foundation), this.transform, -Vector3.forward * TILE_WIDTH ),
                new SlotForBuildable( typeof(Foundation), this.transform, Vector3.right * TILE_WIDTH ),
                new SlotForBuildable( typeof(Foundation), this.transform, -Vector3.right * TILE_WIDTH ),
                new SlotForBuildable( typeof(Stairs), this.transform, Vector3.zero )
            };
        }

        protected override void Placement_OnGround_SetTransform(RaycastHit chosenHit)
        {
            transform.position = chosenHit.point + Vector3.up * .5f;
        }

        protected override void Placement_Apply(RaycastHit chosenHit, Buildable chosenBuildable, SlotForBuildable chosenSlot)
        {
            if (chosenSlot.isFree)// the reference for foundations it is not really necesary (i think), but i already made it so i have a starting point, when destroying stuff
            {
                AutoFillFoundationSlots();
            }
        }
        // ========================= OTHER

        protected void AutoFillFoundationSlots()
        {
            foreach (var slot in slots)
            {
                if (slot.acceptedType != typeof(Foundation))
                {
                    continue;
                }
                Collider[] colliders = OverlapsColliders(slot.worldPosition, .4f);
                if (colliders.Length == 0)
                {
                    continue;
                }
                Foundation otherFoundation = null;
                foreach (var otherCollider in colliders)
                {
                    if (otherCollider == this.collider)
                    {
                        continue;
                    }
                    otherFoundation = otherCollider.GetComponent<Foundation>();
                    if (otherFoundation != null)
                    {
                        ConnectFoundationsReferences(this, otherFoundation);
                    }
                }
            }
        }

        public override void Demolish()
        {
            Destroy(this.gameObject);

            foreach (var slot in slots)
            {
                if (slot.obj == null || slot.acceptedType == typeof(Foundation))
                {
                    continue;
                }
                slot.obj.Demolish();
            }
        }

        #region  commented out: Old PlacementFunction
        // /// <summary>Returns wheter it can be placed or not</summary>
        // public bool OLD_Placement(RaycastHit[] hits, out System.Action OnApplyPlacementCallback)
        // {
        //     if (hits.Length == 0)
        //     {
        //         OnApplyPlacementCallback = null;
        //         return false;
        //     }

        //     // Get correct hit
        //     bool hittedGround = true;
        //     RaycastHit chosenHit = hits[0];
        //     Foundation chosenComponent = null;

        //     foreach (var hit in hits)
        //     {
        //         if (hit.collider == this.collider)
        //         {
        //             continue;
        //         }
        //         // Check for component
        //         chosenComponent = hit.collider.GetComponent<Foundation>();
        //         if (chosenComponent != null)
        //         {
        //             hittedGround = false;
        //             chosenHit = hit;
        //             break;
        //         }
        //         // Check for ground
        //         if (hit.collider.gameObject.layer == Layers.intGround)
        //         {
        //             chosenHit = hit;
        //         }
        //     }

        //     Debug.DrawLine(chosenHit.point, chosenHit.point + Vector3.up, Color.red);

        //     // SET POSITION ON GROUND
        //     if (hittedGround)
        //     {
        //         transform.position = chosenHit.point + Vector3.up * .5f;
        //         OnApplyPlacementCallback = null;

        //         Collider[] colliders = OverlapsColliders(transform.position, .55f);
        //         if (colliders.Length == 0)
        //         {
        //             return false;
        //         }
        //         foreach (var otherCollider in colliders)// searchs for ground
        //         {
        //             if (otherCollider == this.collider)
        //             {
        //                 continue;
        //             }
        //             if (otherCollider.gameObject.layer != Layers.intGround)// if is overlaping with something that is not ground
        //             {
        //                 return false;
        //             }
        //         }
        //         return true;
        //     }
        //     // SNAP TO OTHER FOUNDATION
        //     else
        //     {
        //         transform.rotation = chosenComponent.transform.rotation;
        //         SlotForBuildable chosenSlot = chosenComponent.GetNearestFoundationSlot(chosenHit.point);
        //         transform.position = chosenSlot.worldPosition;

        //         // checks if position is free, excludes from the check the Buildable that this is gonna be attached to
        //         float sizeFactor = .45f;// a little more small than .5f so, it doesn't touch stuff and i don't have to program more stuff
        //         Collider[] colliders = OverlapsColliders(transform.position, sizeFactor);
        //         foreach (var otherCollider in colliders)
        //         {
        //             var otherBuildable = otherCollider.gameObject.GetComponent<Buildable>();
        //             if (otherCollider == this.collider || otherBuildable == null || otherBuildable == chosenComponent)
        //             {
        //                 continue;
        //             }
        //             OnApplyPlacementCallback = null;
        //             return false;
        //         }

        //         OnApplyPlacementCallback = () =>
        //         {
        //             if (chosenSlot.isFree)// the reference for foundations it is not really necesary (i think), but i already made it so i have a starting point, when destroying stuff
        //             {
        //                 // chosenSlot.gameObject = this.gameObject;
        //                 // // Add a reference of the chosenCompoenent to this
        //                 // BuildableSlot this_slot = this.GetNearestFoundationSlot(chosenComponent.transform.position);
        //                 // this_slot.gameObject = chosenComponent.transform.gameObject;
        //                 AutoFillSlots();
        //             }
        //         };

        //         return chosenSlot.isFree;
        //     }
        // }
        #endregion
    }//end of class
}//end of namespace