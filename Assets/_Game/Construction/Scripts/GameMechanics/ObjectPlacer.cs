﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Networking;

namespace ConstructionSystem
{
    public class ObjectPlacer : NetworkBehaviour
    {
        [SerializeField] KeyCode placementActiveButton = KeyCode.Mouse2;
        [SerializeField] KeyCode guiActiveButton = KeyCode.B;
        [SerializeField] KeyCode placeButton = KeyCode.Mouse0;
        [SerializeField] KeyCode removeButton = KeyCode.Mouse1;
        [Space(10)]
        [SerializeField]
        BuildingLevels currentBuildingLevel = BuildingLevels.Twig;
        [SerializeField] BuildingObjects currentBuildingObject = BuildingObjects.Foundation;

        [HideInInspector] public Camera cam;
        new Collider collider;
        Buildable beingPlaced;

        //========================

        void Start()
        {
            collider = GetComponent<Collider>();
            cam = GetComponentInChildren<Camera>();
        }

        //========================

        bool placementActive;
        bool guiActive;

        void OnDestroy()
        {
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
        }

        void Update()
        {
            //Debug.Log("ObjectPlacer::Update() -- hasAuthority: " + hasAuthority + ", netId: " + netId);
            if (hasAuthority == false)
            {
                return;
            }

            #region pressing buttons, hiding cursor, showing gui and thins of those sorts
            if (Input.GetKeyDown(placementActiveButton))
            {
                placementActive = !placementActive;
            }
            if (Input.GetKeyDown(guiActiveButton))
            {
                guiActive = !guiActive;
            }
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                Cursor.lockState = CursorLockMode.None;
                Cursor.visible = true;
                placementActive = false;
                guiActive = false;
            }
            if (guiActive == false)
            {
                if (Input.GetMouseButtonDown(0) || Input.GetKeyUp(guiActiveButton))
                {
                    Cursor.lockState = CursorLockMode.Locked;
                    Cursor.visible = false;
                }
            }
            else
            {
                Cursor.lockState = CursorLockMode.None;
                Cursor.visible = true;
                return;
            }
            #endregion// pressing buttons

            if (placementActive == false || Cursor.lockState != CursorLockMode.Locked)
            {
                if (beingPlaced != null)
                {
                    Destroy(beingPlaced.gameObject);
                }
                return;
            }

            float rayDistance = 10;
            Ray ray = new Ray(cam.transform.position, cam.transform.forward);//= cam.ScreenPointToRay(Input.mousePosition);
            RaycastHit[] hits = Physics.RaycastAll(ray, rayDistance);

            //Debug.DrawRay(ray.origin, ray.direction * rayDistance, Color.red);

            if (beingPlaced == null)
            {
                GetInstanceOfObjectBeingPlaced();
            }

            System.Action ApplyPlacement;
            ObjectPlacementManager.PositionateBuildable(hits, beingPlaced, this.transform.position, out ApplyPlacement);

            bool canBePlaced = (ApplyPlacement != null);

            if (canBePlaced && Input.GetKeyUp(placeButton))
            {
                ApplyPlacement();
                CmdSpawnBuildableBeingPlaced( currentBuildingLevel, currentBuildingObject, beingPlaced.transform.position, beingPlaced.transform.rotation );
                Destroy(beingPlaced.gameObject);
            }

            if (Input.GetKeyUp(removeButton))
            {
                ObjectPlacementManager.RemoveClickedBuildable(hits, this.collider);
            }
        }

        //========================

        void GetInstanceOfObjectBeingPlaced()
        {
            if (beingPlaced != null)
            {
                Destroy(beingPlaced.gameObject);
                beingPlaced = null;
            }

            GameObject prefab = BuildingPrefabs.GetPrefab(currentBuildingLevel, currentBuildingObject);
            var go = (GameObject)MonoBehaviour.Instantiate(prefab);
            beingPlaced = go.GetComponent<Buildable>();

            beingPlaced.GetComponents();
            beingPlaced.SetColliderEnabled(false);
            beingPlaced.Init(currentBuildingLevel, currentBuildingObject);
        }

        //========================

        [Command]
        void CmdSpawnBuildableBeingPlaced(BuildingLevels lvl, BuildingObjects obj, Vector3 position, Quaternion rotation) // NOTE: What about reference of things attached 
        {
            GameObject prefab = BuildingPrefabs.GetPrefab(lvl, obj);
            
            var go = (GameObject)MonoBehaviour.Instantiate(prefab);
            var buildable = go.GetComponent<Buildable>();

            buildable.Init(lvl, obj);
            buildable.transform.position = position;
            buildable.transform.rotation = rotation;

            NetworkServer.Spawn(go);
        }

        [Command]
        void CmdSetCurrentLevelAndObject(BuildingLevels lvl, BuildingObjects obj)
        {
            currentBuildingLevel = lvl;
            currentBuildingObject = obj;
            RpcSetCurrentLevelAndObject(currentBuildingLevel, currentBuildingObject);
        }

        [ClientRpc]
        void RpcSetCurrentLevelAndObject(BuildingLevels lvl, BuildingObjects obj)
        {
            if (hasAuthority)
            {
                return;
            }
            currentBuildingLevel = lvl;
            currentBuildingObject = obj;
        }
        //========================

        void OnGUI()
        {
            if (hasAuthority == false)
            {
                return;
            }

            GUI.Button(new Rect(5, 125, 215, 100), "W,A,S,D & SPACE.\nMiddle Click for building.\nB for choosing what to build.\nLeft cick to place it.\nRight click to delete it.");

            if (guiActive == false)
            {
                return;
            }

            DrawButtonsForChoosingWhatToBuild();
        }

        void DrawButtonsForChoosingWhatToBuild()
        {
            float w = 150;
            float h = 20;
            float m = 5;
            float x = Screen.width * .5f - w * .5f;
            float yStart = Screen.height * .25f;

            string[] names = System.Enum.GetNames(typeof(BuildingObjects));
            var values = System.Enum.GetValues(typeof(BuildingObjects));

            for (int i = 0; i < names.Length; i++)
            {
                float y = yStart + h * i + m * (i + 1);
                if (GUI.Button(new Rect(x, y, w, h), names[i]))
                {
                    //Debug.Log("Switch to " + names[i]);
                    currentBuildingObject = (BuildingObjects)values.GetValue(i);
                    CmdSetCurrentLevelAndObject(currentBuildingLevel, currentBuildingObject);
                    GetInstanceOfObjectBeingPlaced();
                    guiActive = false;
                    placementActive = true;
                    Cursor.lockState = CursorLockMode.Locked;
                    Cursor.visible = false;
                }
            }
        }
    }//end of class
}//end of namespace
