﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ConstructionSystem
{
    public enum BuildingLevels
    {
        Twig=0, Wood, Stone, Metal
    }

    [System.Serializable]
    public class BuildingPrefabs
    {
        public static BuildingPrefabs ins;
        public static GameObject GetPrefab(BuildingLevels level, BuildingObjects obj)
        {
            //Debug.Log("Get prefab: " + level + " " + obj);
            return ins.GetGroup(level).GetPrefab(obj);
        }

        [SerializeField] BuildingPrefabsSet twig;
        [SerializeField] BuildingPrefabsSet wood;
        [SerializeField] BuildingPrefabsSet stone;
        [SerializeField] BuildingPrefabsSet metal;

        public GameObject GetPrefab_(BuildingLevels level, BuildingObjects obj)
        {
            //Debug.Log("Get prefab: " + level + " " + obj);
            return GetGroup(level).GetPrefab(obj);
        }

        BuildingPrefabsSet GetGroup(BuildingLevels level)
        {
            BuildingPrefabsSet prefabSet = null;

            switch (level)
            {
                case BuildingLevels.Twig:
                    prefabSet = twig;
                    break;
                case BuildingLevels.Wood:
                    prefabSet = wood;
                    break;
                case BuildingLevels.Stone:
                    prefabSet = stone;
                    break;
                case BuildingLevels.Metal:
                    prefabSet = metal;
                    break;
            }

            return prefabSet;
        }
    }// end of calls

    public enum BuildingObjects
    {
        Foundation=0, FoundationSteps, Floor, Wall, DoorFrameSingle, DoorFrameDouble, WindowFrame, Stairs
    }

    [System.Serializable]
    class BuildingPrefabsSet
    {
        [SerializeField] GameObject foundation;
        [SerializeField] GameObject foundationSteps;
        [Space(10)]
        [SerializeField] GameObject floor;
        [Space(10)]
        [SerializeField] GameObject wall;
        [SerializeField] GameObject doorFrameSingle;
        [SerializeField] GameObject doorFrameDouble;
        [SerializeField] GameObject windowFrame;
        [Space(10)]
        [SerializeField] GameObject stairs;

        public GameObject GetPrefab(BuildingObjects obj)
        {
            GameObject prefab = null;

            switch (obj)
            {
                case BuildingObjects.Foundation:
                    prefab = foundation;
                    break;
                case BuildingObjects.FoundationSteps:
                    prefab = foundationSteps;
                    break;

                case BuildingObjects.Floor:
                    prefab = floor;
                    break;

                case BuildingObjects.Wall:
                    prefab = wall;
                    break;
                case BuildingObjects.DoorFrameSingle:
                    prefab = doorFrameSingle;
                    break;
                case BuildingObjects.DoorFrameDouble:
                    prefab = doorFrameDouble;
                    break;
                case BuildingObjects.WindowFrame:
                    prefab = windowFrame;
                    break;
                
                case BuildingObjects.Stairs:
                    prefab = stairs;
                    break;
            }

            return prefab;
        }
    }//end of class
}// end of namespace