﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

namespace ConstructionSystem
{
    public class BuildingPrefabsMono : MonoBehaviour
    {
        [SerializeField] BuildingPrefabs buildingPrefabs;
        
        NetworkManager networkManager;

        void Start()
        {
            BuildingPrefabs.ins = buildingPrefabs;
            networkManager = FindObjectOfType<NetworkManager>();
            AddPrefabsToNetworkManager();
        }

        void AddPrefabsToNetworkManager()
        {
            if (networkManager == null)
            {
                Debug.LogError("Missing Network Manager");
				return;

            }

            foreach (var lvl in System.Enum.GetValues(typeof(BuildingLevels)))
            {
                foreach (var obj in System.Enum.GetValues(typeof(BuildingObjects)))
                {
                    GameObject prefab = BuildingPrefabs.GetPrefab((BuildingLevels)lvl, (BuildingObjects)obj);
                    if (prefab != null)
                    {
                        networkManager.spawnPrefabs.Add(prefab);
                    }
                }
            }
        }
    }// end of class
}// end of namespace