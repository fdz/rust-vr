﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ConstructionSystem
{
    public static class ObjectPlacementManager
    {
        static Material matCanNotPlace = Resources.Load("PlacementColors/matCanNotPlace") as Material;
        static Material matCanPlace = Resources.Load("PlacementColors/matCanPlace") as Material;

        /// <summary>returns wether the object was placed or not</summary>
        public static void PositionateBuildable(RaycastHit[] hits, Buildable beingPlaced, Vector3 characterPosition, out System.Action ApplyPlacement)
        {
            ApplyPlacement = null;

            if (beingPlaced == null)
            {
                Debug.LogError("Tried to place a NULL Buildable");
                return;
                //return false;
            }
            beingPlaced.SetColliderEnabled(false);

            if (hits.Length == 0)
            {
                beingPlaced.gameObject.SetActive(false);
                //return false;// raycast did not hit anything
            }
            else
            {
                beingPlaced.gameObject.SetActive(true);
            }

            beingPlaced.transform.rotation = Quaternion.Euler(new Vector3(0, Quaternion.LookRotation(beingPlaced.transform.position - characterPosition).eulerAngles.y, 0));
            System.Action OnApplyPlacement = null;
            bool canBePlaced = beingPlaced.Placement(hits, out OnApplyPlacement);

            if (canBePlaced)
            {
                beingPlaced.SetMaterials(matCanPlace);
                // if ( userPressedKey )
                // {
                //     ApplyPlacement();
                // }
                ApplyPlacement = () =>
                {
                    beingPlaced.SetColliderEnabled(true);
                    beingPlaced.ResetMaterials();
                    if (OnApplyPlacement != null)
                    {
                        OnApplyPlacement();
                    }
                };
            }
            else
            {
                beingPlaced.SetMaterials(matCanNotPlace);
            }
            //return false;
        }

        public static void RemoveClickedBuildable( RaycastHit[] hits, Collider ignore )
        {
            Buildable buildable = null;
            foreach (var hit in hits)
            {
                buildable = hit.collider.GetComponent<Buildable>();
                if ( buildable == null || hit.collider == ignore )
                {
                    continue;
                }
                break;
            }

            if (buildable != null)
            {
                buildable.Demolish();
            }
        }
    }// end of class
}// end of namespace