﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityStandardAssets.Characters.FirstPerson;

namespace Name
{
    ///<summary>A thing that can move || Based on [quill18 - Unity MULTIPLAYER Tutorial -- Episode 4](https://www.youtube.com/watch?v=D7kxrpNCTyU)</summary>
    public class MovingEntity : NetworkBehaviour
    {
        [SerializeField] float speed = 5;
        [SerializeField] float jumpSpeed = 10;
        Vector3 velocity;
        Vector3 bestGuessPosition;

        float ourLatency;// TODO: This should probably something we get from the PlayerConnectionObject
        float latencySmoothingFactor = 10;

        [SerializeField] Transform _camPosInitier;
        [SerializeField] GameObject camPrefab;
        Camera cam;
        MouseLook mouseLook;

        void Start()
        {
            //Debug.Log("MovingEntity::Start() -- isClient: " + isClient + ", isServer: " + isServer);
            if (hasAuthority == false)
            {
                MeshRenderer mr = GetComponentInChildren<MeshRenderer>();
                mr.material.color = Color.red;
            }

            float p = 10;
            transform.position = new Vector3(Random.Range(-p, p), .2f, Random.Range(-p, p));
            
            //gameObject.AddComponent<ConstructionSystem.ObjectPlacer>();
        }  

        bool startedAuthoritation;
        public override void OnStartAuthority()
        {
            //Debug.Log("MovingEntity::OnStartAuthority()");
            startedAuthoritation = true;

            MeshRenderer mr = GetComponentInChildren<MeshRenderer>();
            mr.material.color = Color.blue;

            cam = Instantiate(camPrefab).GetComponent<Camera>();
            cam.transform.parent = _camPosInitier.transform.parent;
            cam.transform.position = _camPosInitier.position;

            mouseLook = new MouseLook();
            mouseLook.Init(transform, cam.transform);

            // adde other components
            //gameObject.AddComponent<ConstructionSystem.ObjectPlacer>();
            ConstructionSystem.ObjectPlacer objPlacer = GetComponent<ConstructionSystem.ObjectPlacer>();
            objPlacer.cam = cam;
        }

        void Update()
        {
            if (hasAuthority == false || startedAuthoritation == false)
            {
                bestGuessPosition = bestGuessPosition + (velocity * Time.deltaTime);
                transform.position = Vector3.Lerp(transform.position, bestGuessPosition, Time.deltaTime * latencySmoothingFactor);
                return;
            }

            // if we get to here, we are the authoritative owner of this object
            transform.Translate(velocity * Time.deltaTime);

            if (true)//some input
            {
                velocity = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
                velocity.Normalize();
                velocity *= speed;
                velocity.y = (Input.GetButtonDown("Jump") ? 1 : 0) * jumpSpeed;
                CmdUpdateVelocity(velocity, transform.position, transform.rotation);
            }

            if ( Cursor.lockState == CursorLockMode.Locked )
            {
                float oldYRotation = transform.eulerAngles.y;
                mouseLook.LookRotation(transform, cam.transform);

                if (true)//(m_IsGrounded || advancedSettings.airControl)
                {
                    // Rotate the rigidbody velocity to match the new direction that the character is looking
                    Quaternion velRotation = Quaternion.AngleAxis(transform.eulerAngles.y - oldYRotation, Vector3.up);
                    //m_RigidBody.velocity = velRotation*m_RigidBody.velocity;
                }
            }
        }

        [Command]
        void CmdUpdateVelocity(Vector3 v, Vector3 p, Quaternion r)// minimize information, like sending a float for velocity or something like that
        {
            velocity = v;
            transform.position = p;
            transform.rotation = r;

            // now let the clients know the correct position of this object
            RpcUpdateVelocity(velocity, transform.position, r);// latency "doubles up" since now the server is sending information to the clients
        }

        [ClientRpc]
        void RpcUpdateVelocity(Vector3 v, Vector3 p, Quaternion r)
        {
            // on a client
            if (hasAuthority)
            {
                // depending on the game, i MIGHT want to change to match this info
                // from the server, even though that might look a little wonky to the user.
                return;
            }
            // i am a non-authoratative client, so i definetly need to listen to the server

            velocity = v;
            bestGuessPosition = p + (v * (ourLatency));
            transform.rotation = r;
        }
    }// end of class
}// end of namespace