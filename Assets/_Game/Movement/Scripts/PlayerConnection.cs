﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

namespace Name
{
	public class PlayerConnection : NetworkBehaviour
	{
		[SerializeField] GameObject characterPrefab;

		[SerializeField] GameObject myCharacter;
		
		void Start()
		{
			if ( isLocalPlayer == false )
			{
				return;
			}
			
			CmdSpawnMyUnit();
		}

		void Update()
		{
			if ( isLocalPlayer == false )
			{
				return;
			}
		}

		[Command]
        void CmdSpawnMyUnit( )
        {
			//Debug.Log("Spawn Character");
            // We are guaranteed to be on the server right now.
            GameObject go = (GameObject)Instantiate(characterPrefab);

            // now that the object exists on the server, propagate it to all
            // the client (and also wire up the network identity)
			//NetworkServer.Spawn(go);
            NetworkServer.SpawnWithClientAuthority(go, connectionToClient);
			
			//go.GetComponent<NetworkIdentity>().AssignClientAuthority( connectionToClient );
			//Debug.Log( go.GetComponent<MovingEntity>().hasAuthority );
			//go.GetComponent<MovingEntity>().Init( isLocalPlayer );
        }
	}// end of class
}// end of namespace